/*
 * create by ZZZubec
 * @create: 16/11/2012
 * @modifed: 21/12/2012
 */
package ru.salamandr.dev.utils;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import ru.salamandr.dev.utils.LogSystem;

public class SQLSystem
{
    private LogSystem log;
    private Statement sql;

    private DataBaseConfig db;


    public EnumSQL sqlType = EnumSQL.SQLLite;

	public enum EnumSQL
	{
		SQLLite, MySQL;
	}

    public SQLSystem( LogSystem log, DataBaseConfig db, EnumSQL type )
    {
        this.log = log;
        this.db = db;
        this.sqlType = type;
    }

    public boolean connectSQL()
    {
        Connection conn = null;
        try
		{
		    log.showMessage( null, "connectSQL", "initialize" );
            if( sqlType == EnumSQL.SQLLite )
            {
                conn = DriverManager.getConnection("jdbc:sqlite:" + this.db.file );
                sql = conn.createStatement();
                if( sql != null )
                    return true;
            }
            else
            {
            	Class.forName("org.gjt.mm.mysql.Driver");
                conn = DriverManager.getConnection("jdbc:mysql://" + this.db.host + ":" + this.db.port + "/" + this.db.name, this.db.username, this.db.password );
                sql = conn.createStatement();
                if( sql != null )
                    return true;
            }
		} catch (SQLException e) {
		    log.errorMessage( "SQLSystem", "connectSQL->SQLException", e.getMessage() );
			e.printStackTrace();
		} catch (Exception e) {
		    log.errorMessage( "SQLSystem", "connectSQL->Exception", e.getMessage() );
			e.printStackTrace();
		}
        return false;
    }

    public ResultSet query( String query )
    {
        ResultSet rs = null;

        if( sql == null )
        {
            if( !this.connectSQL() ) return rs;
        }

        try
        {
            log.showMessage( null, "SQLSystem->query", query );
            return sql.executeQuery( query );
        } catch (SQLException e) {
            log.errorMessage( null, "SQLSystem->query", e.getMessage() );
			e.printStackTrace();
        }
        return rs;
    }

    public boolean update( String query )
    {
        if( sql == null )
        {
            if( !this.connectSQL() ) return false;
        }

        try
        {
            log.showMessage( null, "SQLSystem->update", query );
            sql.executeUpdate( query );
            return true;
        } catch (SQLException e) {
            log.errorMessage( null, "SQLSystem->query", e.getMessage() );
			e.printStackTrace();
        }
        return false;
    }

	public void close() {
		// TODO Auto-generated method stub
		try {
			sql.close();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
